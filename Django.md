# Install Django

    $ pip install django

# Django基本操作

> 新建

    $ django-admin startproject web

> 啟動開發用即時Server

    $ python manage.py runserver

> 新建一個app

    $ django-admin startapp app

> 預設127.0.0.1:8000
> 結束control + c

> manage.py django指令程式
> __init__ 將目錄設為python package
> urls.py django網站路由設定
> setting.py django設定檔

# Django設定檔簡單說明
*   是否開啟除錯模式

        DEBUG=<Bool>

*   DEBUG=False要設定允許的IP或Domain

        ALLOWED_HOSTS = []

*   安裝django套件或功能

        INSTALLED_APPS = [  
           'django.contrib.admin',  
           'django.contrib.auth',  
           'django.contrib.contenttypes',  
           'django.contrib.sessions',  
           'django.contrib.messages',  
           'django.contrib.staticfiles',   
           
           'app',
        ]

*   語系

        LANGUAGE_CODE = 'zh-hant'  
*   時區

        TIME_ZONE = 'Asia/Taipei'
