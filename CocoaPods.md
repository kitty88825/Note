# CocoaPods 

* [Install](#Install)
* [Get Started](#Get-Started)
* [Create a pod](#Create-a-pod)
* [參考相關教學網站](#參考相關教學網站)

如果你的gem版本比較低，可以在terminal裡輸入如下命令列來更新Ruby：

    $ sudu gem update --system
## Install

    $ sudo gem install cocoapods
## Get Started
在檔案位置開啟終端機執行(記得切換路徑)

    $ pod init

使用vim或者其他開啟 Podfile(這裡使用vim)

    $ vim Podfile
開啟後輸入需要安裝的套件，如無法輸入按Ｉ鍵


      pod 'AFNetworking', '~> 2.6'
      pod 'ORStackView', '~> 3.0'
      pod 'SwiftyJSON', '~> 2.3'
輸入完後按esc退出輸入，畫面呈現如下方

    platform :ios, '8.0'
    use_frameworks!

    target 'MyApp' do
      pod 'AFNetworking', '~> 2.6'
      pod 'ORStackView', '~> 3.0'
      pod 'SwiftyJSON', '~> 2.3'
    end
執行後輸入 :wq 儲存並退出

    :wq
最後執行安裝套件
    
    $ pod install
## Create a pod
目前能力不足，請參考

  [如何建立一個屬於自己的CocoaPods]: http://google.com/
  [如何建立一個屬於自己的CocoaPods][]

## 參考相關教學網站
> Cocoapods官網 https://cocoapods.org/
>
> 程序員的後花園https://com-it.tech/archives/103401
