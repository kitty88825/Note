# command

* [終端機設定忽略大小寫](#終端機設定忽略大小寫)

## 終端機設定忽略大小寫
終端機輸入

    nano .inputrc
再輸入

    set completion-ignore-case on
    set show-all-if-ambiguous on
    TAB: menu-complete
control + O 儲存
