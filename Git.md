# Git

GitHub：https://github.com/

bitbucket：https://bitbucket.org/

GitLab：https://gitlab.com/users/sign_in

# Install
Git：https://git-scm.com/download/mac

## 開始
  
    $ git init             # 初始化這個目錄，讓Git對這個目錄開始進行版控
    $ git status           # 查詢現在這個目錄的「狀態」
    $ git add .
    $ git commit -m "___"
    
    $ git remote add origin --位址--   # 初始要設定
    $ git push -u origin master       # 初始要設定
    
    $ git push

## 新增gitignore

    $ touch .gitignore
    $ vim .gitignore
    $ :wq          # 儲存&離開

## 無更改也能commit
只要加上 --allow-empty 參數，沒東西也是可以 Commit

    $ git commit --allow-empty -m "空的"   
## 非兩段式commit
  
    $ git commit -a -m "update"    
## 檢視紀錄

    $ git log
    
Commit：作者是誰

Author：什麼時候 Commit 的

Date：每次的 Commit 大概做了些什麼事

加上額外參數，可以看到不一樣的輸出格式，例如可加上 --oneline 跟 --graph：
    
    $ git log --oneline --graph


## github建一個倉庫

    echo "# NTUB-Python" >> README.md
    git init
    git add README.md
    git commit -m "first commit"
    git remote add origin https://github.com/kitty88825/NTUB-Python.git
    git push -u origin master

## 開分支

    $ git branch    # 後面沒接任何參數，它僅會印出目前在這個專案有哪些分支
    $ git branch cat   # 後面加上分支名稱就新增一個cat分支
    $ git branch -d cat     #刪除分支cat
## 切換分支
    
    $ git checkout cat
    $ git log --oneline   ## 檢視git紀錄
